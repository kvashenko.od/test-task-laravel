<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('users.index', [
            'users' => $users
        ]);
    }


    public function create()
    {
        return view('users.create');
    }


    public function store(UserRequest $request)
    {
        $data=$request->all();
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        return redirect()->route('users.index')->with('success', 'product create successfully');

    }


    public function edit($id)
    {
        $user = User::find($id);
        return view(('users.edit'), [
            'user'=>$user
        ] );

    }


    public function update(UserRequest $request, $id)
    {
        $data=$request->all();
        $user = User::find($id);
        $user ->update([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        return redirect()->route('users.index');

    }


    public function destroy($id)
    {
        User::destroy($id);
        return redirect()->route('users.index');
    }



    public function show($id)
    {
        //
    }
}
