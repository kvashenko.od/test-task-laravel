@extends('layouts.app')

@section('content')

    <h1>Form to delete  user &laquo;{{ $user->name }}&raquo;</h1>
    <div class="card">
        <div class="card-body">

            <form action="{{ route('users.destroy', $user->id) }}" method="post">
                @csrf
                @method('DELETE')

                <div class="mb-3">
                    <label for="name" class="form-label">User's name</label>
                    <input class="form-control" name="name"  value="{{ $user->name }}"></input>
                </div>

                <div class="mb-3">
                    <label for="email" class="form-label">User's email</label>
                    <input class="form-control" name="email" value="{{ $user->email }}"></input>
                </div>



            </form>

            <h3>&#x21e6; To GO BACK, please push: <a href="{{ route('users.index') }}" type="button" class="btn btn-warning">GO BACK</a></h3>

        </div>
    </div>


    <div class="container">
        <div class="card">
            <div class="card-body">
                <!-- Форма удаления пользователя -->
                <form action="{{ route('users.destroy', $user->id) }}" method="post" >
                    <h3>&#x2718; Delete user: &laquo;{{ $user->name }}&raquo;? Please push:
                        @csrf
                        @method('DELETE')

                    </h3>
                </form>
            </div>
        </div>
    </div>


@endsection
