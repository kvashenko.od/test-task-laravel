@extends('layouts.app');
@section('content')

<div class="container">
    <div class="card">
        <div class="card-body">
            <h1>USERS LIST </h1>
            <table class="table table-dark">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>User's name</th>
                    <th>User's email</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    @foreach($users as $user)

                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email}} </td>
                        <td>
                            <div class="btn-group">
                                <form action="{{ route('users.destroy', $user->id) }}" method="post" >

                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">DELETE</button>
                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-info" >Edit user</a>
                                </form>
                            </div>
                        </td>
                </tr>

                @endforeach
                </tbody>
            </table>
            <form method="get" action="{{route('users.create')}}">
                <button  class="btn btn-dark" >Add new user</button>
            </form>
        </div>
    </div>
</div>

@endsection

