@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>FORM to addition a  new user</h1>
        <div class="card">
            <div class="card-body">


                <form action="{{ route('users.store') }}" method="post">
                    @csrf
                    <div class="mb-3">
                        <label for="name" class="form-label">User's name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"  value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>

                    <div class="mb-3">
                        <label for="email" class="form-label">User's email</label>
                        <input class="form-control @error('email') is-invalid @enderror" name="email"  rows="3" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input class="form-control @error('password') is-invalid @enderror" name="password"  rows="3" value="{{ old('password') }}" required autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-dark" >CREATE</button>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-footer">
                <a href="{{ route('users.index') }}" type="button" class="btn btn-primary">BACK</a>
            </div>
        </div>
    </div>
@endsection
