@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>FORM TO EDIT data user &laquo;{{ $user->name }}&raquo;</h1>
        <div class="card">
            <div class="card-body">
                <!-- Форма редактирования данных пользователя -->
                <form action="{{ route('users.update', $user->id) }}" method="post">
                    @method('PUT')
                    @csrf
                    <div class="mb-3">
                        <label for="name" class="form-label">User's name</label>
                        <input type="text" class="form-control" name="name"  value="{{ $user->name }}">
                    </div>

                    <div class="mb-3">
                        <label for="email" class="form-label">User's email</label>
                        <input class="form-control" name="email" value="{{ $user->email }}">
                    </div>

                    <div class="mb-3">
                        <label for="password" class="form-label">User's password</label>
                        <input class="form-control" name="password" value="{{ $user->password }}">
                    </div>

                    <button type="submit" class="btn btn-dark" >UPDATE</button>
                </form>

            </div>
        </div>

        <div class="card">
            <div class="card-footer">
                <form action="{{ route('users.destroy', $user->id) }}" method="post" >
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">DELETE</button>
                    <a href="{{ route('users.index')}}" type="button" class="btn btn-primary">GO BACK</a>
                </form>
            </div>
        </div>

    </div>

@endsection
